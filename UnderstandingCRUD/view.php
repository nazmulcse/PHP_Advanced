<?php
  include_once 'lib/app.php';
  include_once 'vendor/autoload.php';
  use Example\Crud\Profile;
  $profiles=new Profile();
  $data=$profiles->get($_GET['id']);
?>


<!DOCTYPE html>
<html>
<head lang="en">
    <meta charset="UTF-8">
    <title></title>
</head>
<body>

<h1>Your Details</h1>
<dl>
    <dt>Your Full Name</dt>
    <dd><?php echo $data['fullname'];?></dd>

    <dt>Your Email</dt>
    <dd><?php echo $data['email'];?></dd>
</dl>
<nav>
    <li><a href="index.php">List</a> </li>
    <li><a href="create.php">Create</a> </li>
    <li><a href="#">Edit</a> </li>
</nav>
</body>
</html>