<?php
 include_once 'lib/app.php';
include_once 'vendor/autoload.php';
use Example\Crud\Profile;
$profile=new Profile();
$data=$profile->get($_GET['id']);
?>

<!DOCTYPE html>
<html>
<head lang="en">
    <meta charset="UTF-8">
    <title>Understanding CRUD : Edit</title>
</head>
<body>
<section>
    <form action="store.php" method="post">
        <input type="hidden" name="id" value="<?php echo $_GET['id'];?>">
        <h1>EDIT</h1>
        <p>Information Goes Here</p>
        <fieldset>
            <ul>
                <li>
                    <label for="fullname">Enter Your Full Name</label>
                    <input type="text" name="fullname" placeholder="eg. hasan" id="email" value="<?php
                    if(array_key_exists('fullname',$data) && !empty($data['fullname'])) {
                        echo $data['fullname'];
                     }
                      ;?>" autofocus>
                </li>
                <li>
                    <label for="email">Enter Your Email</label>
                    <input type="text" name="email" placeholder="eg. Nazmul Hasan" id="name" value="<?php
                    if(array_key_exists('email',$data) && !empty($data['email'])) {
                        echo $data['email'];
                    }
                    ?>" autofocus>
                </li>
            </ul>
            <input type="submit" name="btn" value="Submit"/>
            <input type="reset" name="btn" value="Reset"/>
        </fieldset>
    </form>
    <a href="index.php">Back</a>
</section>
</body>
</html>