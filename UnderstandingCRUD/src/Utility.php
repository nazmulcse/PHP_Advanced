<?php
/**
 * Created by PhpStorm.
 * User: Nazmul Hasan
 * Date: 01-Jul-15
 * Time: 11:14 PM
 */

namespace Example;


class Utility {
    public function debug($data)
    {
        echo '<pre>';
        print_r($data);
    }
    static function redirect($url='index.php'){
        header('Location:'.$url);
    }

} 