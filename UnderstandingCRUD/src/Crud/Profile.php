<?php
namespace Example\Crud;
use Example\Utility;
class Profile
{
    public $email = '';
    public $fullname = '';
    public $data='';
    private $conn='';

    public function __construct(){
        $this->conn = new \PDO('mysql:host=localhost;dbname=crud', 'root', '');
       $this->conn->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
    }
    public function create()
    {
        $query = "INSERT INTO profiles (email,fullname) VALUES ('$_POST[email]','$_POST[name]')";
        mysql_query($query);
        Utility::redirect();
    }

   public function get($id)
    {
        try {
            $stmt = $this->conn->prepare('SELECT * FROM profiles WHERE id = :id');
            $stmt->execute(array('id' => $id));
            $row = $stmt->fetch();
            return $row;
        } catch(PDOException $e) {
            echo 'ERROR: ' . $e->getMessage();
        }
    }

    public function all()
    {
        try {
            $query = "SELECT * FROM profiles";
            $_result=$this->conn->query($query);
            foreach($_result as $row) {
                $this->data[]=$row;
            }
        } catch(PDOException $e) {
            echo 'ERROR: ' . $e->getMessage();
        }

        return $this->data;
    }

    public function store($data)
    {
        try {
            $stmt = $this->conn->prepare('UPDATE profiles SET fullname = :fname,email=:email WHERE id = :id');
            $stmt->execute(array(
                ':id'   => $data['id'],
                ':fname' => $data['fullname'],
                ':email' => $data['email']
            ));
        } catch(PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
        Utility::redirect();
    }

    public function delete($id)
    {
        try {
            $stmt = $this->conn->prepare('DELETE FROM profiles WHERE id = :id');
            //$stmt->bindParam(':id', $id); // this time, we'll use the bindParam method
           // $stmt->execute();
            $stmt->execute(array('id' => $id));
        } catch(PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
        Utility::redirect();
    }
}