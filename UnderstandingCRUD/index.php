<?php
  include_once 'vendor/autoload.php';
  include_once 'lib/app.php';
  use Example\Crud\Profile;
  $obj=new Profile();
  $data=$obj->all();
?>
<!DOCTYPE html>
<html>
<head lang="en">
    <meta charset="UTF-8">
    <title></title>
</head>
<body>
    <p><a href="create.html">Click Here</a>To add email</p>
    <table border="1">
       <tr>
           <th>SL</th>
           <th>Email</th>
           <th>Name</th>
           <th>Action</th>
       </tr>
        <?php
          if(!empty($data))
          {
          foreach($data as $key=>$value)
          {
        ?>
        <tr>
            <td><?php echo $value['id'];?></td>
            <td>
                <?php
                if(array_key_exists('email',$value) && !empty($value['email'])) {
                    echo $value['email'];
                }
                else
                   echo "Not Provided";
                ?>
            </td>
            <td><?php
                if(array_key_exists('fullname',$value) && !empty($value['fullname'])) {
                    echo $value['fullname'];
                }
                else
                    echo "Not Provided";
                ?>
            </td>
            <td>
                <a href="view.php?id=<?php echo $value['id'];?>">View</a>
                <a href="edit.php?id=<?php echo $value['id'];?>">Edit</a>
                <a href="delete.php?id=<?php echo $value['id'];?>">Delete</a>
            </td>
        </tr>
        <?php } }
        else{
        ?>
        <tr>
            <td colspan="4">Not Defined</td>
        </tr>
        <?php }?>
    </table>
</body>
</html>