<?php
function all(){
    if(array_key_exists('myemails',$_SESSION) && !empty($_SESSION['myemails']))
        return $_SESSION['myemails'];
}
function create(){
    if(!array_key_exists('myemails',$_SESSION))
    {
        $_SESSION['myemails']=array();
    }
    if(array_key_exists('email',$_POST) && !empty($_POST))
    {
        $_SESSION['myemails'][]=$_POST;
    }
    else
    {
        $_SESSION['myemails']="Not Defined";
    }
    header('Location:index.php');
}
function get(){
    return $_SESSION['myemails'][$_GET['id']];
}
function store(){
    if(array_key_exists('myemails',$_SESSION))
    {
        if(array_key_exists('email',$_POST) && !empty($_POST))
        {
            $_SESSION['myemails'][$_POST['id']]=$_POST;
        }
        else
        {
            $_SESSION['myemails']="Not Defined";
        }
    }
    header('Location:index.php');
}
function delete(){
    unset($_SESSION['myemails'][$_GET['id']]);
    header('Location:index.php');
}